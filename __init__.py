"""
This is a set of Python classes which helps analyzing vibrational spectra and QM calculations
"""

from .helpers import * # some functions are useful to have in the analysis script
from .MassPhoto import MP_data
from .CircDichro import CD_data
from .MST import MST_data
from .MassSpec import MS_data
from .DLS import DLS_data

